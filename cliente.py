class Cliente:

    def __init__(self, nome, documento, inadimplente=False):
        self.__nome = nome
        self.__documento = documento
        self.__inadimplente = inadimplente

    def is_inadimplente(self):
        if(self.__inadimplente):
            print("{} é inadimplente.".format(self.__nome))
        else:
            print("{} não é inadimplente.".format(self.__nome))

    @property
    def nome(self):
        return self.__nome.title()

    @nome.setter
    def nome(self, nome):
        self.__nome = nome

    @property
    def documento(self):
        return self.__documento

    @property
    def inadimplente(self):
        return self.__inadimplente
