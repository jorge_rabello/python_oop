from banco import cria_conta, deposita, saca, extrato

conta1 = cria_conta(123, "Jorge Rabello", 200.00, 6000)

print(conta1["numero"])

extrato(conta1)
deposita(conta1, 50)
extrato(conta1)
saca(conta1, 10)
extrato(conta1)

# embora devessemos depositar apenas pela função deposita
# no paradigma procedural que estamos utilizando nada impede isso
conta1["saldo"] = 100
extrato(conta1)

# também podemos criar uma conta sem todas as informações
conta2 = {"numero": 456, "limite": 200.0}

# deposita(conta2, 300) isso resultaria em erro
