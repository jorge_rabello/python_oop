class Retangulo:

    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        self.__area = x * y

    def obter_area(self):
        return self.__area


retangulo = Retangulo(7, 6)
retangulo.area = 7
print(retangulo.obter_area())
