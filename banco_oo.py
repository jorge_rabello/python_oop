from cliente import Cliente
from conta import Conta

jorge = Cliente("Jorge Rabello", 000, True)
conta = Conta(123, jorge, 100.00, 50.00)

conta.__Conta__saldo = 900  # não surte nenhum efeito

conta.extrato()  # Saldo do titular Jorge Rabello R$ 100.0

conta.deposita(200)
conta.extrato()

conta.saca(50)
conta.extrato()


maria = Cliente("Maria Rabello", 999)
conta2 = Conta(456, maria, 300.00, 10000.00)
conta2.extrato()

conta2.transfere(100, conta)
conta2.extrato()
conta.extrato()

jorge.is_inadimplente()
maria.is_inadimplente()

print(conta.saldo)
print(conta.limite)

conta.limite = 300
print("LIMITE {}".format(conta.limite))
print("SALDO {}".format(conta.saldo))
print("VALOR DISPONÍVEL {}".format(conta.saldo + conta.limite))

conta.saca(20000)
print("LIMITE {}".format(conta.limite))
print("SALDO {}".format(conta.saldo))
print("VALOR DISPONÍVEL {}".format(conta.saldo + conta.limite))

conta.saca(650)
print("LIMITE {}".format(conta.limite))
print("SALDO {}".format(conta.saldo))
print("VALOR DISPONÍVEL {}".format(conta.saldo + conta.limite))

print(Conta.codigo_banco())
print(Conta.codigos_bancos())
codigos = Conta.codigos_bancos()

print(codigos['Caixa'])
print(codigos['BB'])
